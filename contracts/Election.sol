pragma solidity ^0.4.2;

contract Election {
  // Model a Candidate
  struct Candidate {
    uint id;
    string name;
    uint voteCount;
    uint points;
  }

  // Read/write candidates
  mapping(uint => Candidate) public candidates;

  // Store accounts that have voted
  mapping(address => mapping(uint => bool)) public voters;
  
  // Store Candidates Count
  uint public candidatesCount;

  constructor() public {
    addCandidate("ALINE");
    addCandidate("ARTHUR");
    addCandidate("BRENNO");
    addCandidate("BRUNO");
    addCandidate("CARLOS");
    addCandidate("ENEIAS");
    addCandidate("FELIPE");
    addCandidate("GABRIELGONCALVES");
    addCandidate("GABRIELMOUZELLA");
    addCandidate("GUSTAVOGARCIA");
    addCandidate("GUSTAVOGLASGIO");
    addCandidate("KELVIN");
    addCandidate("LEANDRO");
    addCandidate("LUCAS");
    addCandidate("LUIZOTAVIO");
    addCandidate("RAFAEL");
    addCandidate("RODRIGOBITTENCOURT");
    addCandidate("RODRIGOCALDEIRA");
    addCandidate("WILLIAN");
  }

  function addCandidate (string _name) private {
    candidatesCount ++;
    candidates[candidatesCount] = Candidate(candidatesCount, _name, 0, 0);
  }

  function vote (uint _candidateId, uint8 value) public {
    // require that they haven't voted before
    require(!voters[msg.sender][_candidateId]);

    // require a valid candidate
    require(_candidateId > 0 && _candidateId <= candidatesCount);
    
    require(value >= 0 && value <= 3);

    // record that voter has voted
    voters[msg.sender][_candidateId] = true;

    // update candidate vote Count
    candidates[_candidateId].points += value;
    candidates[_candidateId].voteCount += 1;
  
  }
}
